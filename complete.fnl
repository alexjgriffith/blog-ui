;; Complete
(local lg love.graphics)

(local colours
       {
        :text       [ 0.2109375 0.30859375 0.41796875 0.99609375 ]
        :button     [ 0.9375 0.9375 0.9375 0.99609375 ]
        :background [ 0.26171875 0.86328125 0.8984375 0.99609375 ]
        :highlight  [ 0.984375 0.31640625 0.51953125 0.99609375 ]})

(local (bw bh) (values 200 50))
(local (window-w window-h) (love.window.getMode))

(local button-font (lg.newFont "inconsolata.otf" 24))

(local beep (love.audio.newSource "beep.ogg" :static))

(fn point-within [px py x y w h]
  ;; px < x + w  and px > x and py < y + h and py > y
  (and (< px (+ x w))
       (> px x)
       (< py (+ y h))
       (> py y)))

(var over nil)
(var rotation 0)
(var count-1 0)
(var count-2 0)

(var first-previous nil)
(fn first-only [x]
  (let [first (~= x first-previous)]
    (set first-previous x)
    (when first x)))

(fn draw []
  (local (mx my) (love.mouse.getPosition))
  (lg.clear colours.background)
  (do
    (lg.push :all)
    (lg.translate (-> window-w (- bw) (/ 2) (math.floor))
                  (-> window-h (- bh) (/ 2) (math.floor)))
    (lg.translate (/ bw 2) (/ bh 2))
    ;; (lg.rotate rotation)
    (lg.translate (/ bw -2) (/ bh -2))
    (local (screen-x screen-y) (lg.inverseTransformPoint mx my))
    (lg.setColor colours.button)
    (lg.rectangle :fill 0 0 bw bh)
    (local hover (point-within screen-x screen-y 0 0 bw bh))
    (when hover (set over :button-1))
    (if hover
        (lg.setColor colours.highlight)
        (lg.setColor colours.text))
    (lg.setLineWidth 6)
    (lg.rectangle :line 0 0 bw bh)
    (lg.setFont button-font)
    (lg.printf (.. "B1 Clicked " count-1) 0 10 bw :center)
    (lg.pop))
  (when true
    (lg.push :all)
    (lg.translate (-> window-w (- bw) (/ 2) (math.floor))
                  (-> window-h (- bh) (/ 2) (math.floor)))
    (lg.translate (/ bw 2) (/ bh 2))
    (lg.rotate (* rotation 1.5))
    (lg.translate (/ bw -2) (/ bh -2))
    (local (screen-x screen-y) (lg.inverseTransformPoint mx my))
    (lg.setColor colours.button)
    (lg.rectangle :fill 0 0 bw bh)
    (local hover (point-within screen-x screen-y 0 0 bw bh))
    (when hover
      (set over :button-2))
    (if hover
        (lg.setColor colours.highlight)
        (lg.setColor colours.text))
    (lg.setLineWidth 6)
    (lg.rectangle :line 0 0 bw bh)
    (lg.setFont button-font)
    (lg.printf (.. "B2 Clicked " count-2) 0 10 bw :center)
    (lg.pop)))

(fn update [dt]
  (match (first-only over)
    nil (do :no-hover)
    _ (beep:play))
  (set over nil) ;; handlers -> update -> draw
  (set rotation (+ rotation  dt)))

(fn mousereleased [x y button]
  (match over
    :button-1 (set count-1 (+ count-1 1))
    :button-2 (set count-2 (+ count-2 1))))

{: mousereleased : update : draw :name "Complete"}
