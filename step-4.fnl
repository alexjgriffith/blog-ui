;; Step 4 -- click button
(local lg love.graphics)

(local colours
       {
        :text       [ 0.2109375 0.30859375 0.41796875 0.99609375 ]
        :button     [ 0.9375 0.9375 0.9375 0.99609375 ]
        :background [ 0.26171875 0.86328125 0.8984375 0.99609375 ]
        :highlight  [ 0.984375 0.31640625 0.51953125 0.99609375 ]})

(local (bw bh) (values 200 50))
(local (window-w window-h) (love.window.getMode))

(fn point-within [px py x y w h]
  ;; px < x + w  and px > x and py < y + h and py > y
  (and (< px (+ x w))
       (> px x)
       (< py (+ y h))
       (> py y)))

(local button-font (lg.newFont "inconsolata.otf" 24))
(var over nil)
(var count 0)
(var rotation 0)

(fn draw []
  ;; get mouse position
  (local (mx my) (love.mouse.getPosition))
  ;; clear frame
  (lg.clear colours.background)
  (lg.setColor colours.button)
  ;; move to position
  (lg.translate (-> window-w (- bw) (/ 2) (math.floor))
                (-> window-h (- bh) (/ 2) (math.floor)))
  ;; add some rotation for flair
  (lg.translate (/ bw 2) (/ bh 2))
  (lg.rotate (* rotation 5))
  (lg.translate (/ bw -2) (/ bh -2))
  ;; transform mouse to screen space
  (local (screen-x screen-y) (lg.inverseTransformPoint mx my))
  ;; determine if mouse is within button
  (local hover (point-within screen-x screen-y 0 0 bw bh))
  (when hover (set over :button))
  ;; draw button
  (lg.setColor colours.button)
  (lg.rectangle :fill 0 0 bw bh)
  ;; set text and outline colour to pink when hovered
  (if hover
      (lg.setColor colours.highlight)
      (lg.setColor colours.text))
  (lg.setLineWidth 6)
  (lg.rectangle :line 0 0 bw bh)
  (lg.setFont button-font)
  (lg.printf (.. "Clicked " count) 0 10 bw :center))

(fn update [dt]
  (set over nil) ;; handlers -> update -> draw
  (set rotation (+ rotation  dt)))


(fn mousereleased [x y button]
  (match over
    :button (set count (+ count 1))))

{: mousereleased : update : draw :name "Step 4"}
