## How Not to do UI in LOVE2D

Love2d is a great game framework. It provides all the tools you need to make a game wrapped up in a series of easy to use lua modules. It provides many build in graphical tools to make 2d games, but leaves the development of the UI to the reader.

To fill this void there have been a ton of UI libraries and wrappers written for Love2d, including both pure lua implementations, like [SUIT](https://github.com/vrld/SUIT) and wrappers around c libraries, like [nuklear](https://github.com/keharriso/love-nuklear). These libraries are extensive and, I'd argue for most hobby games, overkill.

In my personal projects I've taken several approaches to implementing UI, including using [bump](https://github.com/kikito/bump.lua) a library that handles axis aligned rectangular collisions and an ill advised implementation of [a UI library of my own](https://gitlab.com/alexjgriffith/anchor.fnl).

For my entry to this year's lisp game jam, [Frozen-Horizon](https://alexjgriffith.itch.io/frozen-horizon) I wanted to take a simpler path. My goal was to stick with the tools love provides out of the box and try and create an immediate mode GUI (no GUI state). This post details my trials and tribulations.

## Drawing a Button
Love2d has built in procedures for rendering simple geometry, like rectangles. To draw a rectangle at point 0 0 that is 100px wide and 200 px tall you can call the `love.graphics.rectangle`.

``` fennel
(love.graphics.rectangle :fill 0 0 100 200)
```
The keyword `:fill` specifies that the rectangle should be filled in. The other option is `:line` which specifies that only the outline should be drawn.

To set the colour of the button we can use `love.graphics.setColor`, which takes the red, green, blue and alpha args, ranging from 0 to 1 either as individual arguments or as part of a table. Note, this will change the colour of everything you draw.

```fennel
;; Set the colour to white
(let [(r g b a) (values 1 1 1 1)]
    (love.graphics.setColor r g b a))

;; Set the colour to black
(let [(r g b a) (values 0 0 01)]
    (love.graphics.setColor r g b a))
```

To draw text we can use the function `love.graphics.print` or `love.graphics.printf`. The later can be used to centre text horizontally.


``` fennel
(let [(x y w) (values 0 100 300)]
    (love.graphics.printf "Some Text" x y w :center))
```

Lets choose some colours and put it all together, check out a ux colour pallet site for some inspiration like [colorsinspo](https://colorsinspo.com/). I'll be using:

``` fennel


(local colours
       {
        :text       [ 0.2109375 0.30859375 0.41796875 0.99609375 ]
        :button     [ 0.9375 0.9375 0.9375 0.99609375 ]
        :background [ 0.26171875 0.86328125 0.8984375 0.99609375 ]
        :highlight  [ 0.984375 0.31640625 0.51953125 0.99609375 ]})
```

Now lets go about drawing a button. We will use the functions discussed above.

``` fennel
(local lg love.graphics)

(local (bw bh) (values 200 50))

(fn draw []
  ;; clear frame
  (lg.clear colours.background)
  (lg.setColor colours.button)
  ;; draw button
  (lg.rectangle :fill 0 0 bw bh)
  (lg.setColor colours.text)
  (lg.rectangle :line 0 0 bw bh)
  (lg.printf "Button" 0 10 bw :center))
```

You should now have a button in the top right of the left. 

[love.graphics.inverseTransformPoint](https://love2d.org/wiki/love.graphics.inverseTransformPoint)

[matrix inversion](https://github.com/love2d/love/blob/8e7fd10b6fd9b6dce6d61d728271019c28a7213e/src/common/Matrix.cpp)

``` c++
love::Vector2 Transform::inverseTransformPoint(love::Vector2 p)
{
	love::Vector2 result;
	getInverseMatrix().transformXY(&result, &p, 1);
	return result;
}
```

``` lua
function pointWithin(px,py,x,y,w,h)
  return px < x + w  and px > x and
         py < y + h and py > y
end
```

