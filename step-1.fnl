;; Step 1 -- draw button
(local lg love.graphics)

(local colours
       {
        :text       [ 0.2109375 0.30859375 0.41796875 0.99609375 ]
        :button     [ 0.9375 0.9375 0.9375 0.99609375 ]
        :background [ 0.26171875 0.86328125 0.8984375 0.99609375 ]
        :highlight  [ 0.984375 0.31640625 0.51953125 0.99609375 ]})

(local (bw bh) (values 200 50))
(local (window-w window-h) (love.window.getMode))

(fn draw []
  ;; clear frame
  (lg.clear colours.background)
  (lg.setColor colours.button)
  ;; draw button
  (lg.rectangle :fill 0 0 bw bh)
  (lg.setColor colours.text)
  (lg.rectangle :line 0 0 bw bh)
  (lg.printf "Button" 0 10 bw :center))

{: draw :name "Step 1"}
