(var state (require :step-1))

(fn switch-to [req]
  (set state (require req)))

(fn love.update [dt]
  (when state.update
    (state.update dt)))

(local lg love.graphics)
(local default-font (lg.getFont))
(local ui-font (lg.newFont "inconsolata.otf" 20))
(local (window-w window-h) (love.window.getMode))

(fn love.draw []
  (when state.draw
    (state.draw))
  (lg.origin)
  (lg.setFont ui-font)
  (lg.printf state.name 0 (- window-h 30) (- window-w 10) :right)
  (lg.setLineWidth 0)
  (lg.setFont default-font)
  )

(fn love.mousereleased [x y button]
  (when state.mousereleased
    (state.mousereleased x y button)))

(fn love.keypressed [key keycode]
  (match key
    :1 (switch-to :step-1)
    :2 (switch-to :step-2)
    :3 (switch-to :step-3)
    :4 (switch-to :step-4)
    :5 (switch-to :step-5)
    :6 (switch-to :complete)))
